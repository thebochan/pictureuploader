package application.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PictureParam {
    private String url;
    private String ip;
    private String browser;
    private String domain;
    private String date;
}