package application.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class PropertiesConfig {

    private RabbitConnectionProperties rabbitConnectionProperties;

    PropertiesConfig(RabbitConnectionProperties rabbitConnectionProperties){
        this.rabbitConnectionProperties = rabbitConnectionProperties;
    }

    public RabbitConnectionProperties getRabbitConnectionProperties() {
        return rabbitConnectionProperties;
    }

    public void setRabbitConnectionProperties(RabbitConnectionProperties rabbitConnectionProperties) {
        this.rabbitConnectionProperties = rabbitConnectionProperties;
    }
}
