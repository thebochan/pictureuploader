package application.controller;

import application.model.PictureParam;
import application.model.Visit;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.InputStream;

@Controller
public class PictureController {

    private static Logger logger = LogManager.getLogger();

    private final String PICTURE_URL = "http://localhost:8080/picture";

    private AmqpTemplate amqpTemplate;
    private PrometheusMeterRegistry meterRegistry;

    InputStream fileInputStream = PictureController.class.getResourceAsStream("/image/image.jpg");
    byte[] responsePicture = IOUtils.toByteArray(fileInputStream);

    public PictureController(AmqpTemplate amqpTemplate, PrometheusMeterRegistry meterRegistry) throws IOException {
        this.amqpTemplate = amqpTemplate;
        this.meterRegistry = meterRegistry;
    }

    void incrementNewVisitCount() {
        meterRegistry.counter("new_response_count").increment();
    }

    @GetMapping("/picture")
    @ResponseBody
    public ResponseEntity<byte[]> getImage(){
        return ResponseEntity.ok().body(responsePicture);
    }

    @PostMapping("/getpictureurl")
    @ResponseBody
    public String url(PictureParam pictureParam){
        Visit visit = new Visit(pictureParam.getDomain(), pictureParam.getUrl(), pictureParam.getBrowser(), pictureParam.getIp(), pictureParam.getDate());
        amqpTemplate.convertAndSend("newvisit", visit);
        logger.info("New picture request. Info " + visit);
        incrementNewVisitCount();
        return PICTURE_URL;
    }
}